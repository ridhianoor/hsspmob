<?php

namespace App\Http\Controllers;

use App\Models\Authority;
use Illuminate\Http\Request;

class APIController extends Controller
{
   /* public function index()
    {
        $posts = Authority::all();
        return response()->json($posts);
    }*/

    public function index(Request $request)
    {
        $products = Authority::paginate(5);
        return response(array(
            'error' => false,
            'products' =>$products->toArray(),
        ),200);
    }
}
